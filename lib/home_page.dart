import 'package:flutter/material.dart';
import 'package:sifo_pijad/matakuliah_list.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var _appBar = AppBar(
      title: Text(
        'Sifo Pijad',
        style: TextStyle(color: Colors.white),
      ),
    );

    var display1 = Theme.of(context).textTheme.display1;

    var _body = Column(children: <Widget>[
      Text(
        'Selamat Datang',
        style: display1,
      ),
      SizedBox(height: 16.0),
      Text(
          'Ini adalah halaman depan dari aplikasi Sistem informasi permohonan pindah jadwal')
    ]);

    var _fab = FloatingActionButton(
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
      backgroundColor: Colors.red[700],
      //fromRGBO(100, 20, 20, 10.0),
      tooltip: 'Add Floating',
      onPressed: () {},
    );

    var _drawerHeader = DrawerHeader(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue, Colors.blue[100]],
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 80.0),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundColor: Colors.black,
              radius: 24.0,
              child: Image.asset('assets/logo.png'),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text('Nama User (Status)'),
            ),
          ],
        ),
      ),
    );

    var _drawer = Drawer(
      child: ListView(
        children: <Widget>[
          _drawerHeader,
          ListTile(
            title: Text('Halaman Utama'),
            leading: Icon(Icons.home),
            onTap: () {},
          ),
          ListTile(
            title: Text('Permohonan'),
            leading: Icon(Icons.archive),
            onTap: () {},
          ),
          ListTile(
            title: Text('Profil'),
            leading: Icon(Icons.person_pin_circle),
            onTap: () {
              Navigator.of(context).pushNamed(ListMatakuliah.tag);
            },
          ),
          ListTile(
            title: Text('Tentang'),
            leading: Icon(Icons.info),
            onTap: () {},
          ),
          ListTile(
            title: Text('Keluar'),
            leading: Icon(Icons.close),
            onTap: () {},
          ),
        ],
      ),
    );

    return new Scaffold(
      appBar: _appBar,
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: _body,
      ),
      floatingActionButton: _fab,
      drawer: _drawer,
    );
  }
}
