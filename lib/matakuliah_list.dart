import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sifo_pijad/matakuliah.dart';

class ListMatakuliah extends StatelessWidget {
  static String tag = 'list-page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Matakuliah', style: TextStyle(color: Colors.white)),
      ),
      body: StreamBuilder(
        stream: Matakuliah.getData(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            var items = snapshot.data.documents.map((doc) {
              var matakuliah = Matakuliah.fromDocument(doc);
              return ListTile(
                title: Text(matakuliah.nama_matakuliah),
                subtitle: Text(matakuliah.id_matakuliah),
                trailing: Text(matakuliah.sks),
              );
            }).toList();
            if (items.length == 0) {
              return Center(child: Text('Data Matakuliah masih kosong'));
            }
            return ListView.builder(itemBuilder: (_, index) => items[index]);
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
