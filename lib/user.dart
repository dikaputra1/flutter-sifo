import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class User {
  String uid, id_user, nama_lengkap, email, alamat, jenis_kelamin, telephone;
  bool dosen, admin, mahasiswa;

  User(
      {this.id_user,
      this.nama_lengkap,
      this.email,
      this.alamat,
      this.jenis_kelamin,
      this.telephone,
      this.admin,
      this.dosen,
      this.mahasiswa});

  ///fungsi untuk mengubah document firestore ke dalam class User
  User.fromDocument(DocumentSnapshot doc) {
    uid = doc.documentID;
    id_user = doc['id_user'];
    nama_lengkap = doc['nama_lengkap'];
    email = doc['email'];
    alamat = doc['alamat'];
    jenis_kelamin = doc['jenis_kelamin'];
    telephone = doc['telephone'];
  }

  ///fungsi untuk membaca semua data di collection user
  static Stream<QuerySnapshot> getData() {
    return Firestore.instance.collection('user').snapshots();
  }
}
