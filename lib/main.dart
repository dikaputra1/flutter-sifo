import 'package:flutter/material.dart';
import 'package:sifo_pijad/home_page.dart';
import 'package:sifo_pijad/login_page.dart';
import 'package:sifo_pijad/matakuliah_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
    ListMatakuliah.tag: (context) => ListMatakuliah(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sifo-Pijad',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
