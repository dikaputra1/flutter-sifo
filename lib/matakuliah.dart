import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

class Matakuliah {
  String id_matakuliah, nama_matakuliah, sks;

  Matakuliah({this.nama_matakuliah, this.sks});

  Matakuliah.fromDocument(DocumentSnapshot doc) {
    id_matakuliah = doc.documentID;
    nama_matakuliah = doc['nama_matakuliah'];
    sks = doc['sks'];
  }

  static Stream<QuerySnapshot> getData() {
    return Firestore.instance.collection('matakuliah').snapshots();
  }
}
